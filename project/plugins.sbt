// The Typesafe repository
resolvers += "Typesafe repository" at "https://repo.typesafe.com/typesafe/releases/"

// Play Framework
addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.4.1")

// LESS
addSbtPlugin("com.typesafe.sbt" % "sbt-less" % "1.0.0")