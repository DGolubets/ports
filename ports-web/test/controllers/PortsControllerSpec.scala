package controllers

import java.util.UUID

import com.commodityvectors.ports.data.PortRepositoryComponent
import com.commodityvectors.ports.data.model._
import org.scalamock.scalatest.MockFactory
import org.scalatest._
import play.api.test.Helpers._
import play.api.test._

import scala.concurrent.Future

class PortsControllerSpec extends WordSpec with Matchers with MockFactory {

  class TestComponent extends PortsController with PortRepositoryComponent {
    override val portRepository: PortRepository = mock[PortRepository]
  }

  "PortsController" should {

    "get port by id" in {
      val component = new TestComponent
      val fakePort = Port(UUID.randomUUID(), "Port 1", Locode.parse("US NYC").get,
        Polygon(LinearRing(Point(0, 0), Point(1.5, 0), Point(0, 0.5), Point(0, 0))),
        10, 50)

      (component.portRepository.findById(_: UUID)).expects(fakePort.id).returns(Future.successful(Some(fakePort)))
      val result = component.details(fakePort.id.toString).apply(FakeRequest())

      status(result) shouldBe OK
      contentType(result) shouldBe Some("application/json")
    }
  }
}
