package controllers

import java.util.UUID

import com.commodityvectors.ports.data.impl.slick.SlickPortRepositoryComponent
import com.commodityvectors.ports.data.impl.slick.h2.H2DatabaseComponent
import com.commodityvectors.ports.data.model._
import org.scalatest._
import play.api.test.Helpers._
import play.api.test._

class PortsControllerSpec2 extends WordSpec with Matchers {

  val component = new TestComponent

  class TestComponent extends PortsController with
  SlickPortRepositoryComponent with H2DatabaseComponent

  "PortsController" should {

    "get port by id" in {
      val fakePort = Port(UUID.randomUUID(), "Port 1", Locode.parse("US NYC").get,
        Polygon(LinearRing(Point(0, 0), Point(1.5, 0), Point(0, 0.5), Point(0, 0))),
        10, 50)

      await(component.portRepository.create(fakePort))

      val result = component.details(fakePort.id.toString).apply(FakeRequest())

      status(result) shouldBe OK
      contentType(result) shouldBe Some("application/json")

      println(contentAsString(result))
    }
  }
}
