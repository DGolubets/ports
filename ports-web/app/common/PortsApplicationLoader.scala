package common

import play.api.ApplicationLoader
import play.api.ApplicationLoader.Context


/**
 * Created by Dima on 30.06.2015.
 */
class PortsApplicationLoader extends ApplicationLoader {
  override def load(context: Context) = new PortsComponents(context).application
}

