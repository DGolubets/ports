package common

import com.commodityvectors.ports.data.impl.slick.SlickPortRepositoryComponent
import com.commodityvectors.ports.data.impl.slick.h2.H2DatabaseComponent
import controllers.{ApplicationController, PortsController}
import play.api.ApplicationLoader.Context
import play.api.BuiltInComponentsFromContext
import play.api.routing.Router
import router.Routes

/**
 * Created by Dima on 30.06.2015.
 */
class PortsComponents(context: Context) extends BuiltInComponentsFromContext(context) {
  lazy val router: Router = new Routes(httpErrorHandler, cake, cake, assets)
  lazy val assets = new controllers.Assets(httpErrorHandler)
  lazy val cake = new ApplicationController with PortsController with SlickPortRepositoryComponent with H2DatabaseComponent
}
