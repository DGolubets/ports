package models

import com.commodityvectors.ports.data.model.{Locode, Polygon}

/**
 * Port view model.
 * @param id - port identifier that can be skipped when creating a new one
 * @param name - port name
 * @param code - port code in UN/LOCODE format
 * @param polygon - GeoJSON polygon
 * @param maxVesselWidth - a max width for vessels that can enter in meters
 * @param maxVesselLength - a max length for vessels that can enter in meters
 */
case class Port(id: Option[String],
                name: String,
                code: Locode,
                polygon: Polygon,
                maxVesselWidth: Double,
                maxVesselLength: Double)
