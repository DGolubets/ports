package controllers

import java.util.UUID

import com.commodityvectors.ports.data.PortRepositoryComponent
import com.commodityvectors.ports.data.model.Locode
import com.commodityvectors.util.Converter
import controllers.util.{ApiAction, Serialization, Mapping}
import models.Port
import play.api.libs.concurrent.Execution.Implicits._
import play.api.libs.json.Json.JsValueWrapper
import play.api.libs.json._
import play.api.mvc._

import scala.concurrent.Future
import scala.util._

/**
 * Ports REST API controller.
 * Separate trait allows it to be testable.
 */
trait PortsController extends Controller {
  this: PortRepositoryComponent =>

  import Mapping._
  import Serialization._

  /**
   * Lists all ports.
   * @return
   */
  def list() = ApiAction.async {
    portRepository.findAll.map(ports => Ok(Json.toJson(ports.map(toViewModel))))
  }

  /**
   * Lists ports in page range.
   * @return
   */
  def page(pageN: Int) = ApiAction.async {
    val pageSize = 20
    portRepository.find(pageSize * pageN, pageSize).map(ports => Ok(Json.toJson(ports.map(toViewModel))))
  }

  /**
   * Gets port details by id.
   * @param id - port id
   * @return
   */
  def details(id: String) = ApiAction.async {
    Converter.strToUUID(id) match {
      case Success(uuid) =>
        portRepository.findById(uuid) flatMap {
          case Some(port) => Future.successful(Ok(Json.toJson(toViewModel(port))))
          case _ => Future.successful(NotFound)
        }
      case _ => Future.successful(BadRequest(JsonErrorMessage("Invalid arguments format.")))
    }
  }

  /**
   * Creates or updates a port depending on id field presence.
   * @return
   */
  def save = ApiAction.async(BodyParsers.parse.json) { request =>
    request.body.validate[Port].fold(
      errors => Future.successful(BadRequest(JsonErrorMessage(JsError.toJson(errors)))),
      model => {
        if (model.id.isDefined) {
          // update existing port
          val port = fromViewModel(model)
          portRepository.update(port).map(updated =>
            if (updated) Ok(JsonOkMessage("Port has been updated.")) else NotFound)
        }
        else {
          // create a new one
          val port = fromViewModel(model, Some(UUID.randomUUID()))
          portRepository.create(port).map(_ => Ok(JsonOkMessage("Port has been created.", "id" -> port.id)))
        }
      }
    )
  }

  /**
   * Removes a port.
   * @param id - port id
   * @return
   */
  def delete(id: String) = ApiAction.async {
    Converter.strToUUID(id) match {
      case Success(uuid) =>
        portRepository.delete(uuid)
          .map(deleted => if (deleted) Ok("Port deleted!") else NotFound)
      case _ => Future.successful(BadRequest(JsonErrorMessage("Invalid port id.")))
    }
  }

  /**
   * Checks port name availability.
   * @param name
   * @return
   */
  def checkName(name: String) = ApiAction.async {
    portRepository.findByName(name) map (portOpt => Ok(Json.obj("available" -> portOpt.isEmpty)))
  }

  /**
   * Checks port code availability.
   * @param code
   * @return
   */
  def checkCode(code: String) = ApiAction.async {
    val available = Locode.parse(code) match {
      case Success(locode) => portRepository.findByCode(locode) map (portOpt => portOpt.isEmpty)
      case Failure(err) => Future.successful(false)
    }

    available.map(a => Ok(Json.obj("available" -> a)))
  }

  // result helpers

  def JsonOkMessage(message: JsValueWrapper, more: (String, JsValueWrapper)*) = JsonResultMessage("OK", message, more :_*)

  def JsonErrorMessage(message: JsValueWrapper, more: (String, JsValueWrapper)*) = JsonResultMessage("Error", message, more :_*)

  def JsonResultMessage(status: String, message: JsValueWrapper, more: (String, JsValueWrapper)*) = {
    val fields: Seq[(String, JsValueWrapper)] = Seq[(String, JsValueWrapper)]("status" -> "OK", "message" -> message) ++ more
    Json.obj(fields : _*)
  }
}