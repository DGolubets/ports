package controllers.util

import com.commodityvectors.ports.data.model.{Locode, LinearRing, Point, Polygon}
import models.Port
import play.api.libs.functional.syntax._
import play.api.libs.json.Reads._
import play.api.libs.json._

import scala.util.{Failure, Success}

/**
 * JSON serialization implicits.
 */
object Serialization {

  implicit object PointFormat extends Format[Point]{
    override def reads(json: JsValue): JsResult[Point] =
      json.validate[Vector[Double]].flatMap {
        case Vector(lng, lat) => JsSuccess(Point(lng, lat))
        case _ => JsError("Invalid point format.")
      }
    override def writes(p: Point): JsValue = Json.arr(p.lng, p.lat)
  }

  implicit object LinearRingFormat extends Format[LinearRing]{
    override def reads(json: JsValue): JsResult[LinearRing] =
      json.validate[Vector[Point]].map(points => LinearRing(points))
    override def writes(r: LinearRing): JsValue = Json.toJson(r.coordinates)
  }

  implicit object PolygonFormat extends Format[Polygon]{
    override def reads(json: JsValue): JsResult[Polygon] =
      (json \ "coordinates").validate[Vector[LinearRing]].map(rings => Polygon(rings))
    override def writes(poly: Polygon): JsValue =
      Json.obj("type" -> "Polygon", "coordinates" -> poly.rings)
  }

  implicit object LocodeFormat extends Format[Locode]{
    override def reads(json: JsValue): JsResult[Locode] = json.validate[String]
      .flatMap{ s =>
        Locode.parse(s) match {
          case Success(locode) => JsSuccess(locode)
          case Failure(err) => JsError(err.getMessage)
        }
      }
    override def writes(code: Locode): JsValue = JsString(code.toString)
  }

  implicit val portsReads: Reads[Port] = (
    (JsPath \ "id").readNullable[String] and
      (JsPath \ "name").read[String] and
      (JsPath \ "code").read[Locode] and
      (JsPath \ "polygon").read[Polygon] and
      (JsPath \ "maxVesselWidth").read[Double](min(0.0)) and
      (JsPath \ "maxVesselLength").read[Double](min(0.0))
    )(Port.apply _)

  implicit val portsWrites = Json.writes[Port]
}
