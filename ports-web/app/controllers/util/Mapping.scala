package controllers.util

import java.util.UUID

import com.commodityvectors.ports.data.{model => dbm}
import com.commodityvectors.util.Converter
import models.Port

/**
 * Mapping between DB and view models.
 */
object Mapping {

  def fromViewModel(port: Port, id: Option[UUID] = None): dbm.Port =
    dbm.Port(
      id.getOrElse(port.id.map(id => Converter.strToUUID(id).get).get),
      port.name,
      port.code,
      port.polygon,
      port.maxVesselWidth,
      port.maxVesselLength)

  def toViewModel(port: dbm.Port): Port =
    Port(
      Some(port.id.toString),
      port.name,
      port.code,
      port.polygon,
      port.maxVesselWidth,
      port.maxVesselLength)

}
