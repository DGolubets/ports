package controllers

import play.api.mvc._

trait ApplicationController extends Controller {
  def index() = Action {
    Ok(views.html.application.index())
  }
}