/* App Module */

var app = angular.module('app', [
    'ngRoute',
    'portControllers',
    'portServices'
]);

app.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
            when('/ports', {
                templateUrl: 'assets/ng_views/port-list.html',
                controller: 'PortListController'
            }).
            when('/ports/edit/:portId', {
                templateUrl: 'assets/ng_views/port-edit.html',
                controller: 'PortEditController'
            }).
            when('/ports/create', {
                templateUrl: 'assets/ng_views/port-edit.html',
                controller: 'PortEditController'
            }).
            otherwise({
                redirectTo: '/ports'
            });
    }]);
