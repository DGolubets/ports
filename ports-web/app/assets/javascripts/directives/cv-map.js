(function(){
    /**
     * Angular directive: cv-map.
     * Displays a map where a polygon can be drawn and maps its GeoJSON to specified property.
     */
    angular.module('app').directive('cvMap', [function () {
        return {
            scope: {
                polygon: '=polygon'
            },
            link: function (scope, element, attrs) {
                var map = new Map(element[0]);
                var lastPolygonGson;

                map.onPolygonChanged(function (polyGson) {
                    scope.$apply(function () {
                        lastPolygonGson = polyGson;
                        scope.polygon = polyGson;
                    });
                });

                scope.$watch('polygon', function(val){
                    // to prevent circular updates
                    if(lastPolygonGson != val) {
                        map.setPolygonGeoJSON(val);
                    }
                });
            }
        }
    }]);

    /**
     * Some helper tools for GeoJSON and Google maps.
     */
    var googleMapsUtil = {

        /**
         * Converts GoogleMaps Polygon to GeoJSON Polygon.
         * @param polygon GoogleMaps Polygon
         * @returns {{type: string, coordinates: *}}
         */
        polygonToGeoJSON: function (polygon) {
            var coordinates = polygon.getPaths().getArray().map(function (ring) {
                return ring.getArray().map(function (p) {
                    return [p.lng(), p.lat()];
                });
            });

            return {
                type: "Polygon",
                coordinates: coordinates
            };
        },

        /**
         * Converts GeoJSON Polygon to GoogleMaps Polygon.
         * @param geo GeoJSON Polygon
         * @returns GoogleMaps Polygon
         */
        polygonFromGeoJSON: function (geo) {
            var paths = geo.coordinates.map(function (ring) {
                return ring.map(function (p) {
                    return new google.maps.LatLng(p[1], p[0]);
                });
            });

            return new google.maps.Polygon({
                paths: paths,
                editable: true,
                draggable: true
            });
        },

        /***
         * Calculates GoogleMaps Polygon bounds.
         * @param polygon - GoogleMaps Polygon
         * @returns {google.maps.LatLngBounds}
         */
        getPolygonBounds: function (polygon) {
            var bounds = new google.maps.LatLngBounds();
            polygon.getPath().forEach(function (element, index) {
                bounds.extend(element)
            });
            return bounds;
        }
    };

    /**
     * Google map wrapper.
     * Constructs a simple map with drawing tools and allows to draw only one polygon.
     * @param container - DOM element where to place map
     * @constructor
     */
    function Map(container) {
        // private
        var _this = this;
        var _map;
        var _drawingManager;
        var _polygon; // current GoogleMaps Polygon
        var _polygonChangedHandlers = [];
        var _removePolygonButton;


        // init map
        _map = new google.maps.Map(container, {
            center: new google.maps.LatLng(-34.397, 150.644),
            zoom: 8,
            mapTypeControl: true,
            zoomControl: true,
            streetViewControl: false,
            overviewMapControl: false,
            panControl: false
        });

        // add drawing tools
        _drawingManager = new google.maps.drawing.DrawingManager({
            drawingMode: google.maps.drawing.OverlayType.POLYGON,
            drawingControl: true,
            drawingControlOptions: {
                position: google.maps.ControlPosition.TOP_CENTER,
                drawingModes: [
                    google.maps.drawing.OverlayType.POLYGON
                ]
            },
            polygonOptions: {
                editable: true,
                draggable: true
            }
        });
        _drawingManager.setMap(_map);

        // add remove button
        _removePolygonButton = document.createElement('div');
        _removePolygonButton.className = "button-remove-polygon icon icon-danger glyphicon glyphicon-remove";
        _removePolygonButton.title = "Remove polygon";
        _removePolygonButton.index = 1;
        _map.controls[google.maps.ControlPosition.TOP_CENTER].push(_removePolygonButton);
        google.maps.event.addDomListener(_removePolygonButton, 'click', function () {
            setPolygon(null);
        });

        function enableDrawing() {
            _drawingManager.setOptions({
                drawingControl: true
            });
            _drawingManager.setDrawingMode(google.maps.drawing.OverlayType.POLYGON);
            _removePolygonButton.style.display = 'none';
        }

        function disableDrawing() {
            _drawingManager.setDrawingMode(null);
            _drawingManager.setOptions({
                drawingControl: false
            });
            _removePolygonButton.style.display = 'block';
        }

        function polygonChanged() {
            var gson = _this.getPolygonGeoJSON();
            _polygonChangedHandlers.forEach(function (handler) {
                handler(gson);
            });
        }

        function watchPolygon(polygon) {
            // subscribe to changes
            var polygon_path = polygon.getPath();
            google.maps.event.addListener(polygon_path, 'set_at', polygonChanged);
            google.maps.event.addListener(polygon_path, 'insert_at', polygonChanged);
            google.maps.event.addListener(polygon_path, 'remove_at', polygonChanged);
        }

        // sets current polygon
        function setPolygon(polygon, fitMap, noEvent) {
            // only if it changed
            if (polygon != _polygon) {
                if (_polygon) {
                    // remove old polygon from the map
                    _polygon.setMap(null);
                }
                _polygon = polygon;

                if (polygon) {
                    // polygon is set
                    disableDrawing();
                    watchPolygon(polygon);

                    // add polygon to the map
                    polygon.setMap(_map);

                    if(fitMap) {
                        _this.fitToPolygon();
                    }
                }
                else {
                    // polygon is removed - enable drawing tools
                    enableDrawing();
                }

                if(!noEvent) {
                    polygonChanged();
                }
            }
        }

        google.maps.event.addListener(_drawingManager, 'polygoncomplete', function (polygon) {
            setPolygon(polygon);
        });

        // public

        this.fitToPolygon = function () {
            if(_polygon) {
                _map.fitBounds(googleMapsUtil.getPolygonBounds(_polygon));
            }
        };

        this.getPolygonGeoJSON = function (gson) {
            if (_polygon) {
                return googleMapsUtil.polygonToGeoJSON(_polygon);
            }
        };

        this.setPolygonGeoJSON = function (gson) {
            setPolygon(gson ? googleMapsUtil.polygonFromGeoJSON(gson) : undefined, true, true);
        };

        this.onPolygonChanged = function (handler) {
            if (handler) {
                _polygonChangedHandlers.push(handler);
            }
        }
    }
})();
