angular.module('app').directive('cvUniqueValue', ['$q', '$http', function($q, $http) {
    return {
        require: 'ngModel',
        scope:{
          uri: '=cvUniqueValue'
        },
        link: function(scope, elm, attrs, ctrl) {
            var originalValue;

            ctrl.$asyncValidators.cvUniqueValue = function(modelValue, viewValue) {
                if(ctrl.$untouched){
                    originalValue = ctrl.$modelValue;
                }

                if (modelValue == originalValue || ctrl.$isEmpty(modelValue)) {
                    // consider empty model valid
                    return $q.when();
                }

                var def = $q.defer();


                $http.get(scope.uri + modelValue).success(function(data) {
                    if (data.available) {
                        def.resolve();
                    } else {
                        def.reject();
                    }

                });

                return def.promise;
            };
        }
    };
}]);