var portServices = angular.module('portServices', ['ngResource']);

portServices.factory('PortApi', ['$resource',
    function($resource){
        return $resource('api/ports/:id');
    }]);