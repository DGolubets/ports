var portsModule = angular.module('portControllers', []);

portsModule.controller('PortListController', ['$scope', '$http', '$location', 'PortApi',
    function($scope, $http, $location, PortApi) {
        $scope.createPort = function(){
            // navigate to port creation view
            $location.path('/ports/create');
        };

        $scope.editPort = function(port){
            // navigate to port edit view
            $location.path('/ports/edit/' + port.id);
        };

        $scope.removePort = function(port){
            if(confirm("Do you really want to delete '" + port.name + "' port?")){
                PortApi.delete({id: port.id}, function(){
                    queryPorts();
                });
            }
        };

        function queryPorts(){
            $scope.ports = PortApi.query();
        }

        queryPorts();
    }]);

portsModule.controller('PortEditController', ['$scope', '$http', '$routeParams', '$location', 'PortApi',
    function($scope, $http, $routeParams, $location, PortApi) {
        // if we have port id - we are editing an existing port
        if($routeParams.portId) {
            $scope.port = PortApi.get({id: $routeParams.portId});
        }
        else {
            $scope.port = {};
        }

        $scope.cancel = function () {
            // navigate back to ports list
            $location.path('/ports');
        };

        $scope.save = function () {

            $http.post('/api/ports', $scope.port).
                success(function(data){
                    $location.path('/ports');
                }).
                error(function(data, status, headers, config) {
                    alert("Error: " + data);
                });

        };

    }]);