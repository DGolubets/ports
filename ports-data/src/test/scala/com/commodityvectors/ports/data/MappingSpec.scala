package com.commodityvectors.ports.data

import java.util.UUID

import com.commodityvectors.ports.data.impl.mongo.Mapping
import com.commodityvectors.ports.data.model._
import org.scalatest._

class MappingSpec extends WordSpec with Matchers {

  import TryValues._

  "Mapping" should {

    "write Port to BSON and read it back the same" in {
      val port = Port(UUID.randomUUID(), "Port 1", Locode.parse("US NYC").get,
        Polygon(LinearRing(Point(0, 0), Point(1.5, 0), Point(0, 0.5), Point(0, 0))),
        10, 50)

      val bsonPort = Mapping.PortMapper.writeTry(port).success.value
      val portRead = Mapping.PortMapper.readTry(bsonPort).success.value
      port shouldBe portRead

      // It would be nice to check exact BSON result too, to be sure db-only fields like polygon.type are stored correctly.
      // But I need some sort of BSONDocument comparer for that.
    }
  }
}

