package com.commodityvectors.ports.data.impl.slick

/**
 * Created by Dima on 26.06.2015.
 */
trait SlickDatabaseComponent {

  val database: SlickDatabase

  trait SlickDatabase extends SlickDatabaseSchema with SlickDatabaseMapping {
    val driver: slick.driver.H2Driver
    val db: driver.backend.Database
  }
}
