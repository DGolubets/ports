package com.commodityvectors.ports.data.impl.slick

import com.commodityvectors.ports.data.model.{Locode, Polygon}
import slick.driver.H2Driver.api._

/**
 * Created by luigi on 15/06/15.
 */
trait SlickDatabaseMapping {
  this: SlickDatabaseComponent#SlickDatabase =>

  object mappers {

    // And a ColumnType that maps it to Int values 1 and 0
    implicit val locodeColumnType = MappedColumnType.base[Locode, String](
    { c => c.toString }, { str => Locode.parse(str).get }
    )

    // And a ColumnType that maps it to Int values 1 and 0
    implicit val polygonColumnType = MappedColumnType.base[Polygon, String](
    { c => "" }, { str => Polygon() }
    )

  }
}
