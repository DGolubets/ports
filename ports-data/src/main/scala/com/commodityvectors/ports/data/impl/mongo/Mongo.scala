package com.commodityvectors.ports.data.impl.mongo

import com.commodityvectors.util.Configuration
import reactivemongo.api.{MongoConnection, MongoDriver}

/**
 * Mongo manager.
 *
 * ReactiveMongo says that both ReactiveMongo class and MongoConnection class should be instantiated one time in the application.
 * So, I think singleton is OK in this case.
 */
private object Mongo extends Configuration {

  // parse config
  private lazy val configSection = config.getConfig("com.commodityvectors.ports.mongodb")

  // get an instance of the driver
  // (creates an actor system as said in the docs)
  private val driver = new MongoDriver

  // gets a connection (creates a connection pool)
  // let it crash if config value is invalid or missing - it's critical error
  private val connection = driver.connection(MongoConnection.parseURI(configSection.getString("uri")).get)

  /**
   * An execution context for Mongo.
   * Let's use scala default for now.
   */
  implicit val executionContext = scala.concurrent.ExecutionContext.Implicits.global

  /**
   * Gets a reference to the database
   * @param name of the database
   * @return
   */
  def db(name: String) = connection(name)

  /**
   * Posts database
   */
  lazy val defaultDatabase = db(configSection.getString("database"))

}
