package com.commodityvectors.ports.data.impl.mongo

import java.util.UUID

import com.commodityvectors.ports.data.model._
import com.commodityvectors.util.Converter
import reactivemongo.bson._
import reactivemongo.core.commands.LastError

import scala.concurrent.Future

/**
 * Implicit conversions for MongoDB
 */
private[data] object Mapping {

  /**
   * UUID mapper.
   *
   * I want to use UUID as a primary key of my objects to keep model independent of concrete DB.
   * However in MongoDB I have to make an explicit conversion between BSONValue type and UUID.
   * I believe BSONBinary is the best type to store UUID there.
   */
  implicit object UUIDMapper extends BSONHandler[BSONBinary, UUID] {
    // don't bother with Try monads here, since BSONDocumentReader requires us to write straight mapping anyway
    def read(bson: BSONBinary): UUID = Converter.bytesToUUID(bson.value.readArray(16)).get

    def write(id: UUID): BSONBinary = BSONBinary(Converter.uuidToBytes(id), Subtype.UuidSubtype)
  }

  /**
   * Point mapper
   */
  implicit object PointMapper extends BSONHandler[BSONArray, Point] {
    def read(bson: BSONArray): Point =
      Point(
        bson.getAs[Double](0).get,
        bson.getAs[Double](1).get)

    def write(point: Point): BSONArray = BSONArray(point.lng, point.lat)
  }

  /**
   * Point mapper
   */
  implicit object LinearRingMapper extends BSONHandler[BSONArray, LinearRing] {
    def read(bson: BSONArray): LinearRing =
      LinearRing(bson.values.map(_.asInstanceOf[BSONArray].as[Point]).toVector)

    def write(ring: LinearRing): BSONArray =
      BSONArray(ring.coordinates.map(p => BSON.write(p)))
  }

  /**
   * Polygon mapper.
   * Writes it in GeoJSON format.
   */
  implicit object PolygonMapper extends BSONHandler[BSONDocument, Polygon] {
    def read(doc: BSONDocument): Polygon = {
      Polygon(doc.getAs[Vector[LinearRing]]("coordinates").get)
    }

    def write(polygon: Polygon): BSONDocument =
      BSONDocument(
        "type" -> "Polygon",
        "coordinates" -> polygon.rings)
  }

  /**
   * Locode mapper.
   */
  implicit object LocodeMapper extends BSONHandler[BSONString, Locode] {
    def read(bson: BSONString): Locode = Locode.parse(bson.value).get
    def write(code: Locode): BSONString = BSONString(code.toString)
  }

  /**
   * Port mapper
   */
  implicit object PortMapper extends BSONDocumentReader[Port] with BSONDocumentWriter[Port] {
    def read(doc: BSONDocument): Port = {
      Port(
        doc.getAs[UUID]("_id").get,
        doc.getAs[String]("name").get,
        doc.getAs[Locode]("code").get,
        doc.getAs[Polygon]("polygon").get,
        doc.getAs[Double]("maxVesselWidth").get,
        doc.getAs[Double]("maxVesselLength").get)
    }

    def write(port: Port): BSONDocument =
      BSONDocument(
        "_id" -> port.id,
        "name" -> port.name,
        "code" -> port.code,
        "polygon" -> port.polygon,
        "maxVesselWidth" -> port.maxVesselWidth,
        "maxVesselLength" -> port.maxVesselLength)
  }



  /**
   * Helps dealing with MongoDB results.
   */
  implicit class MongoResultMapper(result: Future[LastError]) {

    import Mongo.executionContext

    /**
     * Maps results with LastError indicating error to failure.
     */
    def noError(): Future[LastError] = result flatMap {
      case e if e.inError => Future.failed(new Exception(e.message))
      case _ => result
    }
  }
}

