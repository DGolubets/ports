package com.commodityvectors.ports.data.impl.slick

import java.util.UUID

import com.commodityvectors.ports.data.PortRepositoryComponent
import com.commodityvectors.ports.data.model.{Locode, Port}

import scala.concurrent._

/**
 * H2 port repository.
 */
trait SlickPortRepositoryComponent extends PortRepositoryComponent {
  self : SlickDatabaseComponent =>

  import database.db
  import database.driver.api._
  import database.mappers._
  import database.schema._

  import scala.concurrent.ExecutionContext.Implicits.global

  override lazy val portRepository = new H2PortRepository

  class H2PortRepository extends PortRepository {
    /**
     * Finds all ports.
     * @return port sequence
     */
    override def findAll: Future[Seq[Port]] = {
      val q = for(p <- ports) yield p
      db.run(q.result)
    }

    /**
     * Updates a port.
     * @param port to update
     * @return false if no port was updated (not found by that id)
     */
    override def update(port: Port): Future[Boolean] = {
      val q = for(p <- ports if p.id === port.id) yield p
      val ua = q.update(port)
      db.run(ua).map( n => n > 0)
    }

    /**
     * Finds a port by name.
     * @param name - port name
     * @return some port
     */
    override def findByName(name: String): Future[Option[Port]] = {
      val q = for(p <- ports if p.name === name) yield p
      db.run(q.result.headOption)
    }

    /**
     * Finds a port by id.
     * @param id - port id
     * @return some port
     */
    override def findById(id: UUID): Future[Option[Port]] = {
      val q = for(p <- ports if p.id === id) yield p
      db.run(q.result.headOption)
    }

    /**
     * Finds a port by code.
     * @param code - port code
     * @return some port
     */
    override def findByCode(code: Locode): Future[Option[Port]] = {
      val q = for(p <- ports if p.code === code) yield p
      db.run(q.result.headOption)
    }

    /**
     * Deletes a port.
     * @param id of the port
     * @return false if no port was deleted (not found by that id)
     */
    override def delete(id: UUID): Future[Boolean] = {
      val q = for(p <- ports if p.id === id) yield p
      val da = q.delete
      db.run(da).map(_ > 0)
    }

    /**
     * Finds ports.
     * @return port sequence
     */
    override def find(skipN: Int, takeN: Int): Future[Seq[Port]] = {
      val q = for(p <- ports) yield p
      db.run(q.drop(skipN).take(takeN).result)
    }

    /**
     * Creates a port.
     * @param port to create
     * @return
     */
    override def create(port: Port): Future[Unit] = {
      val q = ports += port
      db.run(q).map(_ => ())
    }
  }
}
