package com.commodityvectors.ports.data.impl.slick

import java.util.UUID

import com.commodityvectors.ports.data.model.{Locode, Polygon, Port}

/**
 * Created by luigi on 15/06/15.
 */
trait SlickDatabaseSchema {
  this: SlickDatabaseComponent#SlickDatabase with SlickDatabaseMapping =>

  object schema {
    import driver.api._
    import mappers._

    class Ports(tag: Tag) extends Table[Port](tag, "Ports") {
      def id = column[UUID]("id", O.PrimaryKey)

      // This is the primary key column
      def code = column[Locode]("code")

      def name = column[String]("name")

      def polygon = column[Polygon]("polygon")

      def maxVesselWidth = column[Double]("maxVesselWidth")

      def maxVesselHeight = column[Double]("maxVesselHeight")

      // Every table needs a * projection with the same type as the table's type parameter
      def * = (id, name, code, polygon, maxVesselWidth, maxVesselHeight) <>(Port.tupled, Port.unapply)
    }

    val ports = TableQuery[schema.Ports]
  }
}
