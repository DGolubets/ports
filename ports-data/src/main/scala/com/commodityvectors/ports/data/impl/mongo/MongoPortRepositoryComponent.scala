package com.commodityvectors.ports.data.impl.mongo

import java.util.UUID
import com.commodityvectors.ports.data.PortRepositoryComponent
import com.commodityvectors.ports.data.model.{Locode, Port}
import reactivemongo.api.QueryOpts
import reactivemongo.bson.BSONDocument

import scala.concurrent.Future

/**
 * MongoDB port repository.
 */
trait MongoPortRepositoryComponent extends PortRepositoryComponent {

  override lazy val portRepository = new MongoPortRepository

  class MongoPortRepository extends PortRepository {

    import Mongo.executionContext
    import Scheme.portsCollection
    import Mapping._

    override def findAll: Future[Seq[Port]] =
      portsCollection.find(BSONDocument()).cursor[Port].collect[List]()

    override def find(skipN: Int = 0, takeN: Int = Int.MaxValue): Future[Seq[Port]] =
      portsCollection.find(BSONDocument()).options(QueryOpts(skipN = skipN)).cursor[Port].collect[List](takeN)

    override def findById(id: UUID): Future[Option[Port]] =
      portsCollection.find(BSONDocument("_id" -> id)).one[Port]

    override def findByCode(code: Locode): Future[Option[Port]] =
      portsCollection.find(BSONDocument("code" -> code)).one[Port]

    override def findByName(name: String): Future[Option[Port]] =
      portsCollection.find(BSONDocument("name" -> name)).one[Port]

    override def create(port: Port): Future[Unit] =
      portsCollection.insert(port).noError.map(_ => ())

    override def update(port: Port): Future[Boolean] =
      // return true when number of affected records > 0
      portsCollection.update(BSONDocument("_id" -> port.id), port).noError.map(_.n > 0)

    override def delete(id: UUID): Future[Boolean] =
      // return true when number of affected records > 0
      portsCollection.remove(BSONDocument("_id" -> id)).noError.map(_.n > 0)
  }
}
