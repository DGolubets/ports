package com.commodityvectors.ports.data.model

import java.util.UUID

/**
 * Port data model.
 * @param id - unique identifier in DB.
 * @param name - port name
 * @param code - port code in UN/LOCODE format
 * @param polygon - set of port coordinates
 * @param maxVesselWidth - a max width for vessels that can enter in meters
 * @param maxVesselLength - a max length for vessels that can enter in meters
 */
case class Port(id: UUID,
                name: String,
                code: Locode,
                polygon: Polygon,
                maxVesselWidth: Double,
                maxVesselLength: Double)