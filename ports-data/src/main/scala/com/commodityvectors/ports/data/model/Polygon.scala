package com.commodityvectors.ports.data.model

/**
 * Polygon like defined in GeoJSON
 * @param rings - polygon rings, first must be the exterior ring and any others must be interior rings or holes
 */
case class Polygon(rings: Vector[LinearRing])

object Polygon {
  /**
   * Convenient factory.
   * @param rings
   * @return
   */
  def apply(rings: LinearRing*): Polygon = Polygon(rings.toVector)
}

/**
 * A LinearRing is closed LineString with 4 or more positions.
 * The first and last positions are equivalent (they represent equivalent points).
 * @param coordinates - points of the ring
 */
case class LinearRing(coordinates: Vector[Point])

object LinearRing {
  /**
   * Convenient factory.
   * @param coordinates
   * @return
   */
  def apply(coordinates: Point*): LinearRing = LinearRing(coordinates.toVector)
}


/**
 * A single goe position.
 * @param lng longitude (= x)
 * @param lat latitude (= y)
 */
case class Point(lng: Double, lat: Double)