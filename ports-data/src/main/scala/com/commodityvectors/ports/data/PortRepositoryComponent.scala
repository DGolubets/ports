package com.commodityvectors.ports.data

import java.util.UUID
import com.commodityvectors.ports.data.model.{Locode, Port}
import scala.concurrent.Future

/**
 * Port repository cake-component.
 */
trait PortRepositoryComponent {

  // allow implementation to belong to any component instance,
  // this allows more flexibility with our cake
  val portRepository: PortRepositoryComponent#PortRepository

  trait PortRepository {
    /**
     * Finds all ports.
     * @return port sequence
     */
    def findAll: Future[Seq[Port]]

    /**
     * Finds ports.
     * @return port sequence
     */
    def find(skipN: Int, takeN: Int): Future[Seq[Port]]

    /**
     * Finds a port by id.
     * @param id - port id
     * @return some port
     */
    def findById(id: UUID): Future[Option[Port]]

    /**
     * Finds a port by code.
     * @param code - port code
     * @return some port
     */
    def findByCode(code: Locode): Future[Option[Port]]

    /**
     * Finds a port by name.
     * @param name - port name
     * @return some port
     */
    def findByName(name: String): Future[Option[Port]]

    /**
     * Creates a port.
     * @param port to create
     * @return
     */
    def create(port: Port): Future[Unit]

    /**
     * Updates a port.
     * @param port to update
     * @return false if no port was updated (not found by that id)
     */
    def update(port: Port): Future[Boolean]

    /**
     * Deletes a port.
     * @param id of the port
     * @return false if no port was deleted (not found by that id)
     */
    def delete(id: UUID): Future[Boolean]
  }
}
