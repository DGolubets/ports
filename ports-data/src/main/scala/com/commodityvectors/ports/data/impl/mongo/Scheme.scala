package com.commodityvectors.ports.data.impl.mongo

import reactivemongo.api.collections.default.BSONCollection
import reactivemongo.api.indexes.{IndexType, Index}

/**
 * Keeps references to collections and ensures indexes.
 */
object Scheme {
  import Mongo.executionContext

  val portsCollection = Mongo.defaultDatabase[BSONCollection]("ports")

  // make name and code unique
  portsCollection.indexesManager.ensure(Index(key = Seq("name" -> IndexType.Ascending), unique = true))
  portsCollection.indexesManager.ensure(Index(key = Seq("code" -> IndexType.Ascending), unique = true))
}
