package com.commodityvectors.ports.data.impl.slick.h2

import com.commodityvectors.ports.data.impl.slick.SlickDatabaseComponent
import com.commodityvectors.util.Configuration
import slick.driver.H2Driver.api._

import scala.concurrent.duration._
import scala.concurrent.Await

/**
 * Created by luigi on 15/06/15.
 */
trait H2DatabaseComponent extends SlickDatabaseComponent with Configuration {
  override lazy val database = new H2Database

  class H2Database extends SlickDatabase {
    override lazy val driver = slick.driver.H2Driver
    override lazy val db = Database.forConfig("com.commodityvectors.ports.h2mem1")

    // ugly hack to create in memory db
    val dbSchema = schema.ports.schema
    try{
      Await.result(db.run(DBIO.seq(dbSchema.drop)), 10.seconds)
    }
    catch {
      case exc: Exception =>
    }
    Await.result(db.run(DBIO.seq(dbSchema.create)), 10.seconds)
  }
}



