package com.commodityvectors.ports.data.model

import scala.util._


/**
 * UN/LOCODE
 * I want to ensure that it's valid and stored in the unified format.
 */
case class Locode private (val countryCode: String, val locationCode: String) {
  override def toString = s"$countryCode $locationCode"
}

object Locode {
  def parse(str: String): Try[Locode] = {
    // http://www.unece.org/fileadmin/DAM/cefact/locode/unlocode_manual.pdf - 3.2.1
    val pattern = "([a-zA-Z]{2})\\s?([a-zA-Z2-9]{3})".r

    // upper case is important
    str.toUpperCase match {
      case pattern(cc, lc) => Success(new Locode(cc, lc))
      case _ => Failure(new Exception("Invalid UN/LOCODE format."))
    }
  }
}