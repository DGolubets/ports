package com.commodityvectors.util

import com.typesafe.config.{ConfigFactory, Config}

/**
 * Configuration mixin.
 *
 * Configuration is a cross-cutting concern. I want it to be very easy to use and to make minimal noise.
 * That's why I made it a mixin instead of a cake component.
 * It uses a singleton config, but it can be overwritten in unit tests.
 */
trait Configuration {
  protected def config: Config = Configuration.global
}

/**
 * Singleton configuration. Use when DI can't be applied.
 */
object Configuration {
  lazy val global = ConfigFactory.load()
}
