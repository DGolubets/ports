package com.commodityvectors.util

import grizzled.slf4j.Logger

/**
 * Logging mixin.
 *
 * I made it a mixin instead of cake component for the same reasons described in Configuration.
 */
trait Logging {
  protected lazy val log = Logger(this.getClass)
}
