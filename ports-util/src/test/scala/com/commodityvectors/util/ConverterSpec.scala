package com.commodityvectors.util

import org.scalatest._
import java.util.UUID

class ConverterSpec  extends WordSpec with Matchers {

  import TryValues._

  "UUID bytes conversion" should {

    "convert to 16 bytes" in {
      val uuid = UUID.randomUUID()
      val bytes = Converter.uuidToBytes(uuid)
      bytes.length shouldBe 16
    }

    "convert bytes back to the same UUID" in {
      val uuid = UUID.randomUUID()
      val bytes = Converter.uuidToBytes(uuid)
      Converter.bytesToUUID(bytes).get shouldBe uuid
    }
  }

  "UUID string conversion" should {
    "ignore dashes" in {
      val uuid = UUID.randomUUID()
      val parsedUUID = Converter.strToUUID(uuid.toString)
      parsedUUID.success.value shouldBe(uuid)
    }
  }

}
