/**
 * Root project.
 *
 * This simple web site could be a one project of course.
 * But for demonstration purposes I separated it in few subprojects,
 * like it's usually done in a real big applications.
 */
lazy val root = Project("ports", file("."))
  .aggregate(util)
  .aggregate(data)
  .aggregate(web)

/**
 * This library should contain the common code used by all other projects.
 */
lazy val util = Project("ports-util", file("ports-util"))
  .settings(utilSettings)

/**
 * Project for data access, i.e. our DAL.
 * For future reuse, e.g. if we decide to spit web and REST api or introduce another backend app which needs the same data.
 */
lazy val data = Project("ports-data", file("ports-data"))
  .dependsOn(util)
  .settings(dataSettings)

/**
 * Our web application.
 */
lazy val web = Project("ports-web", file("ports-web"))
  .dependsOn(util)
  .dependsOn(data)
  .enablePlugins(PlayScala)
  .settings(webSettings)







/**
 * Common settings and dependencies.
 */
lazy val commonSettings = Defaults.coreDefaultSettings ++ Seq(
  scalaVersion := "2.11.6",
  version := "1.0.0",

  resolvers += "Typesafe repository" at "https://repo.typesafe.com/typesafe/releases/",
  libraryDependencies ++= Seq(
 
    // runtime
    "com.typesafe" % "config" % "1.2.1", // config
    "ch.qos.logback" % "logback-classic" % "1.1.1" % "compile", // logger
    "org.clapper" %% "grizzled-slf4j" % "1.0.2", // very nice scala logging facade

    // test
    "org.scalatest" % "scalatest_2.11" % "2.2.4" % "test",
    "org.scalamock" %% "scalamock-scalatest-support" % "3.2" % "test"
  )
)

/**
 * Util settings and dependencies.
 */
lazy val utilSettings = commonSettings ++ Seq(
  // reserved for future
)

/**
 * Data lib settings and dependencies.
 */
lazy val dataSettings = commonSettings ++ Seq(
  libraryDependencies ++= Seq(
    "org.reactivemongo" %% "reactivemongo" % "0.10.5.0.akka23",
    "com.h2database" % "h2" % "1.4.187",
    "com.typesafe.slick" %% "slick" % "3.0.0",
    "org.slf4j" % "slf4j-nop" % "1.6.4"
  )
)

/**
 * Web site settings and dependencies.
 */
lazy val webSettings = commonSettings ++ Seq(
  includeFilter in (Assets, LessKeys.less) := "*.less",
  routesGenerator := InjectedRoutesGenerator,

  libraryDependencies ++= Seq(
    "org.webjars" % "angularjs" % "1.3.15",
    "org.webjars" % "bootstrap" % "3.3.4"
  )
)